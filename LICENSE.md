# Vitrine

_Web site displaying pages & data retrieved dynamically from other sites_

By:

* Xavier Arques <mailto:xavierarques@yahoo.com>
* Christophe Benz <mailto:christophe.benz@jailbreak.paris>
* François Bouchet <mailto:francoisbouchet@gmail.com>
* Emmanuel Raviart <mailto:emmanuel@raviart.com>

Copyright (C) 2018 Xavier Arques, François Bouchet, Paula Forteza & Emmanuel Raviart
Copyright (C) 2019 Christophe Benz & Emmanuel Raviart

https://framagit.org/eraviart/vitrine

> Vitrine is free software; you can redistribute it and/or modify
> it under the terms of the GNU Affero General Public License as
> published by the Free Software Foundation, either version 3 of the
> License, or (at your option) any later version.
>
> Vitrine is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
> GNU Affero General Public License for more details.
>
> You should have received a copy of the GNU Affero General Public License
> along with this program. If not, see <http://www.gnu.org/licenses/>.
